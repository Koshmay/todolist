import {
    GET_TOTAL_COMMENTS,
    SET_TOTAL_COMMENTS,
    SET_LOADING_COMMENTS,
    SET_ERROR_LOADING_COMMENTS,
    SET_UPDATED_COMMENT,
    SKIP_UPDATED_COMMENT,
    REMOVE_COMMENT,
    ADD_COMMENT,
} from "../../constans/commentsConstans";

export const fetchComments = () => ({
    type: GET_TOTAL_COMMENTS,
});

export const setComments = (payload) => ({
    type: SET_TOTAL_COMMENTS,
    payload,
});

export const setCommentsError = (payload) => ({
    type: SET_ERROR_LOADING_COMMENTS,
    payload,
});

export const setCommentsLoading = (payload) => ({
    type: SET_LOADING_COMMENTS,
    payload,
});

export const removeComment = (payload) => ({
    type: REMOVE_COMMENT,
    payload,
});

export const setUpdateComment = (payload) => ({
    type: SET_UPDATED_COMMENT,
    payload,
});

export const skipDataComment = () => ({
    type: SKIP_UPDATED_COMMENT,
});

export const addComment = (payload) => ({
    type: ADD_COMMENT,
    payload,
});
