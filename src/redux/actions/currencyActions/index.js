import {
    SET_DARK_SCHEMA,
    SET_APP_LANGUAGE,
} from "../../constans/currencyConstans";

export const setSchemaAction = (payload) => ({
    type: SET_DARK_SCHEMA,
    payload,
});

export const setAppLanguage = (payload) => ({
    type: SET_APP_LANGUAGE,
    payload,
});
