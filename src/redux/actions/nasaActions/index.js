import {
    FETCH_TOTAL_DATA_NEOS,
    SET_FILTERED_NEOS,
    SET_NEOS_ERROR,
    SET_NEOS_PART,
    SET_NEOS_COUNT,
} from "../../constans/nasaConstans";

export const fetchNeos = (payload) => ({
    type: FETCH_TOTAL_DATA_NEOS,
    payload,
});

export const setFilterNeos = (payload) => ({
    type: SET_FILTERED_NEOS,
    payload,
});

export const setNeosError = (payload) => ({
    type: SET_NEOS_ERROR,
    payload,
});

export const setNeosPart = (payload) => ({
    type: SET_NEOS_PART,
    payload,
});

export const setNeosCount = (payload) => {
    return {
        type: SET_NEOS_COUNT,
        payload,
    };
};
