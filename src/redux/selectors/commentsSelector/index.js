export const getCommentsList = (state) => state.comments.commentsList;
export const getCommentsLoading = (state) => state.comments.loading;
export const getCommentsError = (state) => state.comments.error;

export const getOpenCommentsModal = (state) => state.comments.openUpdatedModal;
export const getUpdatedDataModal = (state) => state.comments.updatedComment;
