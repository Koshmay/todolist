import { createSelector } from "reselect";

export const getTempNeos = (state) => state.nasa.part_neos;

export const getError = (state) => state.nasa.isErrorRequest;

export const getFilteredNeos = (state) => state.nasa.filtered_neos;

export const getCountNeos = (state) => state.nasa.count;

export const getDangerPoints = createSelector(getTempNeos, (tempNeos) => {
    const dangerousPoints = tempNeos.map((neos) =>
        neos.reduce(
            (res, neo) =>
                neo.is_potentially_hazardous_asteroid ? res + 1 : res,
            0
        )
    );

    const [theMostDanger, theDanger] = dangerousPoints.sort((a, b) => b - a);

    return {
        theMostDanger,
        theDanger,
    };
});
