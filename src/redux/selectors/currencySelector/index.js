export const getSchema = (state) => state.currency.darkSchema;
export const getLanguage = (state) => state.currency.appLanguage;
