import {
    // call,
    put,
    // all
} from "@redux-saga/core/effects";
// import axios from "axios";

// import { localUrl } from "../../helpers/constants";
import { mockData } from "../../helpers/mockData";
import {
    putCardsCreator,
    // updateCardColumnCreator,
    removeCardColumnCreator,
    addCardColumnCreator,
    editCardCreator,
} from "../actionCreators/cardsCreators";

// function* getColumnTodos(id) {
//   const res = yield call(axios.get, `${localUrl}/todos?columnId=${id}`);
//   return res.data;
// }

export const getTotalCardsSaga = function*() {
    try {
        /***  json-server ***/
        // const combineData = ({ columns, todos }) =>
        //   columns.map((item, i) => ({
        //     ...item,
        //     todos: todos[i]
        //   }));
        // const todos = yield all(columns.map(({ id }) => call(getColumnTodos, id)));
        // const data = combineData({ columns, todos });
        const data = mockData;

        yield put(putCardsCreator(data));
    } catch (err) {
        console.error(err);
        yield put(putCardsCreator([]));
    }
};

export const updateCardColumnSaga = function*(values) {
    const { column, data } = values.payload;

    try {
        /*** for json-server ***/
        // const { id, columnId } = data;
        // const nextData = {
        //   ...data,
        //   columnId: column
        // };
        //const res = yield call(axios.put, `${localUrl}/todos/${id}`, nextData);
        //yield put(updateCardColumnCreator(res.data, columnId));

        /*** for netlify only ***/
        const { columnId } = data;
        const nextData = {
            ...data,
            columnId: column,
        };
        yield put(removeCardColumnCreator(nextData, columnId));
        yield put(addCardColumnCreator(nextData));
        // console.log("nextData ", nextData)
    } catch (err) {
        console.log("Error", err);
    }
};

export const updateCardValuesSaga = function*(values) {
    const { data, valueText, valueLevel } = values.payload;

    try {
        /*** for json-server ***/
        // const { id } = data;
        // const nextData = {
        //   ...data,
        //   todotext: valueText,
        //   level: valueLevel
        // };
        //yield axios.put(`${localUrl}/todos/${id}`, nextData);
        //yield put(editCardCreator(data, valueText, valueLevel));

        /*** for netlify only ***/
        yield put(editCardCreator(data, valueText, valueLevel));
    } catch (err) {
        console.log("Error", err);
    }
};
