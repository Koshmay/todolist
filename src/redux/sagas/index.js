import { fork, takeEvery, takeLatest } from "@redux-saga/core/effects";
import { GET_CARDS, EDIT_TASK, EDIT_VALUES } from "../constans";
import {
    FETCH_TOTAL_DATA_NEOS,
    SET_FILTERED_NEOS,
} from "../constans/nasaConstans";
import { GET_TOTAL_COMMENTS } from "../constans/commentsConstans";
import {
    getTotalCardsSaga,
    updateCardColumnSaga,
    updateCardValuesSaga,
} from "./cardsSaga";
import { fetchTotalNeosSaga, piesNeosSaga } from "./nasaSaga";
import { fetchTotalCommentsSaga } from "./commentsSaga";

export default function* rootSaga() {
    /*** cards ***/
    yield takeEvery(GET_CARDS, getTotalCardsSaga);
    yield takeEvery(EDIT_TASK, updateCardColumnSaga);
    yield takeEvery(EDIT_VALUES, updateCardValuesSaga);

    /*** neos ***/
    yield fork(fetchTotalNeosSaga, FETCH_TOTAL_DATA_NEOS);
    yield takeLatest(SET_FILTERED_NEOS, piesNeosSaga);

    /*** cards ***/
    yield fork(fetchTotalCommentsSaga, GET_TOTAL_COMMENTS);
}
