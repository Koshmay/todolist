import { all, call, put, select, delay } from "@redux-saga/core/effects";

import {
    setFilterNeos,
    setNeosError,
    setNeosPart,
    setNeosCount,
} from "../actions/nasaActions";
import { getFilteredNeos, getCountNeos } from "../selectors/nasaSelectors";
import {
    getTimeStamp,
    formatDate,
    getTodayDayNumber,
    getMaxDayNumber,
} from "../../helpers/nasaHelpers";
import Axios from "axios";
import { chunk, last } from "lodash";
import { timescroll, apiUrl, apiKey } from "../../helpers/nasaConstans";

// import { mockdata } from "../constans/mock";

export function* getDataRenges({ startDate, endDate }) {
    const response = yield call(
        Axios.get,
        `${apiUrl}=${startDate}&end_date=${endDate}&api_key=${apiKey}`
    );

    return response;
}

export function* fetchTotalNeosSaga() {
    const currentDay = getTodayDayNumber();
    const newDaysRange = chunk(
        Array.from({ length: currentDay }, (_, i) => i + 1),
        7
    ).map((range) => [formatDate(range[0]), formatDate(last(range))]);

    // console.log(currentDay);

    try {
        /*** request ***/
        const response = yield all(
            newDaysRange.map(([startDate, endDate]) =>
                call(getDataRenges, { startDate, endDate })
            )
        );

        const result = response.reduce(
            (acc, item) => Object.assign(acc, item.data.near_earth_objects),
            {}
        );

        const neosCards = Object.keys(result).map((key) => result[key]);
        neosCards.sort((a, b) => getTimeStamp(a) - getTimeStamp(b));

        yield put(setFilterNeos(neosCards));
        yield put(setNeosError(false));

        /*** mock request ***/
        // yield put(setFilterNeos(mockdata));
        // yield put(setNeosError(false));
    } catch (err) {
        console.error(err);
        yield put(setNeosError(err));
    }
}

export function* piesNeosSaga() {
    const neosCards = yield select((state) => getFilteredNeos(state));
    let firstDay = 0,
        lastDay = getMaxDayNumber(),
        time = 0;

    while (true) {
        yield delay(time);
        yield put(setNeosPart(neosCards.slice(firstDay, lastDay)));

        firstDay++;
        lastDay++;
        let count = yield select((state) => getCountNeos(state));
        if (lastDay === getTodayDayNumber() + 1) {
            firstDay = 0;
            lastDay = getMaxDayNumber();
            count = 0;
        }
        yield put(setNeosCount(count + 1));
        if (!time) {
            time = timescroll;
        }
    }
}
