import { put, call, delay } from "@redux-saga/core/effects";
import Axios from "axios";
import { apiCommentsUrl } from "../../helpers/commentsConstans";
import { getPieData } from "../../helpers/commentsConstans";
import {
    setComments,
    setCommentsLoading,
    setCommentsError,
} from "../actions/commentsActions";

export function* fetchTotalCommentsSaga() {
    try {
        /*** get mock comments ***/
        yield put(setCommentsLoading(true));

        const response = yield call(Axios.get, `${apiCommentsUrl}`);
        if (response.status === 200) {
            const pieData = getPieData(response.data);
            yield delay(1000);
            yield put(setComments(pieData));
            yield put(setCommentsError(null));
            yield put(setCommentsLoading(false));
        } else {
            yield put(setCommentsLoading(false));
            yield put(setCommentsError(response));
        }
    } catch (err) {
        yield put(setCommentsLoading(false));
        yield put(setCommentsError(err));
    }
}
