import rootReducer from "../reducer";
import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import {
    generateIdToNewTodo,
    updateCommentId,
    exampleMiddleware,
} from "../middlewares";
import rootSaga from "../sagas";

const sagaMiddleware = createSagaMiddleware();
const middleware = [
    generateIdToNewTodo,
    updateCommentId,
    exampleMiddleware,
    sagaMiddleware,
];

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(...middleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
            window.__REDUX_DEVTOOLS_EXTENSION__()
    )
);

sagaMiddleware.run(rootSaga);

export default store;
