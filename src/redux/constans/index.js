export const ADD_NEW_TODO = "ADD_NEW_TODO";
export const CHANGE_TODO_STATUS = "CHANGE_TODO_STATUS";
export const REMOVE_TODO = "REMOVE_TODO";

export const GET_TOTAL_CARDS = "GET_TOTAL_CARDS";
export const UPDATE_CARD_COLUMN = "UPDATE_CARD_COLUMN";
export const UPDATE_TASK_VALUES = "UPDATE_TASK_VALUES";

export const GET_CARDS = "GET_CARDS";
export const EDIT_TASK = "EDIT_TASK";
export const EDIT_VALUES = "EDIT_VALUES";

export const REMOVE_CARD = "REMOVE_CARD";
export const ADD_CARD = "ADD_CARD";
