export const GET_TOTAL_COMMENTS = "GET_TOTAL_COMMENTS";
export const SET_TOTAL_COMMENTS = "SET_TOTAL_COMMENTS";
export const SET_LOADING_COMMENTS = "SET_LOADING_COMMENTS";
export const SET_ERROR_LOADING_COMMENTS = "SET_ERROR_LOADING_COMMENTS";

export const SET_UPDATED_COMMENT = "SET_UPDATED_COMMENT";
export const REMOVE_COMMENT = "REMOVE_COMMENT";
export const SKIP_UPDATED_COMMENT = "SKIP_UPDATED_COMMENT";
export const UPDATE_COMMENT = "UPDATE_COMMENT";
export const ADD_COMMENT = "ADD_COMMENT";
