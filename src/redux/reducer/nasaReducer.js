import {
    SET_FILTERED_NEOS,
    SET_NEOS_ERROR,
    SET_NEOS_PART,
    SET_NEOS_COUNT,
} from "../constans/nasaConstans";

const initialState = {
    filtered_neos: [],
    part_neos: [],
    isErrorRequest: false,
    count: 0,
};

export function nasaReducer(state = initialState, action) {
    switch (action.type) {
        case SET_FILTERED_NEOS:
            return { ...state, filtered_neos: action.payload };
        case SET_NEOS_ERROR:
            return { ...state, isErrorRequest: action.payload };
        case SET_NEOS_PART:
            return { ...state, part_neos: action.payload };
        case SET_NEOS_COUNT:
            return { ...state, count: action.payload };
        default:
            return state;
    }
}

export default nasaReducer;
