import {
    SET_DARK_SCHEMA,
    SET_APP_LANGUAGE,
} from "../constans/currencyConstans";

const initialState = {
    darkSchema: false,
    appLanguage: "en",
};

export function currencyReducer(state = initialState, action) {
    switch (action.type) {
        case SET_DARK_SCHEMA:
            return { ...state, darkSchema: action.payload };
        case SET_APP_LANGUAGE:
            return { ...state, appLanguage: action.payload };
        default:
            return state;
    }
}

export default currencyReducer;
