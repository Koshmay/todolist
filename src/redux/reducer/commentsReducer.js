import {
    SET_TOTAL_COMMENTS,
    SET_LOADING_COMMENTS,
    SET_ERROR_LOADING_COMMENTS,
    SET_UPDATED_COMMENT,
    SKIP_UPDATED_COMMENT,
    REMOVE_COMMENT,
    ADD_COMMENT,
} from "../constans/commentsConstans";

const initialState = {
    commentsList: [],
    loading: false,
    error: null,
    updatedComment: {},
    openUpdatedModal: false,
};

export function commentsReducer(state = initialState, action) {
    switch (action.type) {
        case SET_TOTAL_COMMENTS:
            return { ...state, commentsList: action.payload };

        case SET_LOADING_COMMENTS:
            return { ...state, loading: action.payload };

        case SET_ERROR_LOADING_COMMENTS:
            return { ...state, error: action.payload };

        case SET_UPDATED_COMMENT:
            return {
                ...state,
                openUpdatedModal: true,
                updatedComment: action.payload,
            };

        case SKIP_UPDATED_COMMENT:
            return { ...state, openUpdatedModal: false, updatedComment: {} };

        case ADD_COMMENT:
            return {
                ...state,
                commentsList: [...state.commentsList, action.payload],
            };

        case REMOVE_COMMENT:
            const sliceCommentsList = [...state.commentsList].filter(
                (item) => item.id !== action.payload
            );
            return { ...state, commentsList: sliceCommentsList };

        default:
            return state;
    }
}

export default commentsReducer;
