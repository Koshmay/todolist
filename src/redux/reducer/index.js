import { combineReducers } from "redux";
import { mainReducer as todos } from "./todosReducer";
import { cardsReducer as cards } from "./cardsReducer";
import { nasaReducer as nasa } from "./nasaReducer";
import { commentsReducer as comments } from "./commentsReducer";
import { currencyReducer as currency } from "./currencyReducer";

export default combineReducers({
    todos,
    counter,
    cards,
    nasa,
    comments,
    currency,
});

function counter(state, action) {
    if (typeof state === "undefined")
        return {
            count: 0,
        };
    return {
        ...state,
        count: state.count + 1,
    };
}
