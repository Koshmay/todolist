import uuid from "uuid/v4";

export const deflist = [
    {
        text: "Buy pen",
        status: false,
        id: uuid(),
    },
    {
        text: "Feed dog",
        status: true,
        id: uuid(),
    },
    {
        text: "Visit neighbors",
        status: false,
        id: uuid(),
    },
];
