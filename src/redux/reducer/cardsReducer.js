import {
    GET_TOTAL_CARDS,
    UPDATE_CARD_COLUMN,
    UPDATE_TASK_VALUES,
    REMOVE_CARD,
    ADD_CARD,
} from "../constans";

export const initialState = {
    cardslist: [],
};

export const cardsReducer = (state, action) => {
    if (typeof state === "undefined") {
        return initialState;
    }

    switch (action.type) {
        case GET_TOTAL_CARDS:
            const totalCardsList = [...state.cardslist, ...action.payload];
            return {
                ...state,
                cardslist: totalCardsList,
            };

        case UPDATE_CARD_COLUMN:
            const { payload } = action;
            const { updatedCard } = payload;
            const updatedId = updatedCard.id;
            const prevColumn = payload.prevColumn - 1;
            const nextColumn = updatedCard.columnId - 1;
            const newList = [...state.cardslist];

            const prevCardsColumn = newList[prevColumn].todos.filter((item) => {
                return item.id !== updatedId;
            });
            const nextCardsColumn = [...newList[nextColumn].todos, updatedCard];

            newList[prevColumn].todos = prevCardsColumn;
            newList[nextColumn].todos = nextCardsColumn;

            return {
                ...state,
                cardslist: newList,
            };

        case UPDATE_TASK_VALUES:
            const edit_data_card = action.payload;

            const editedCard = edit_data_card.updatedCard;
            const { text, numb } = edit_data_card;

            const editList = [...state.cardslist];
            const editColumn = editedCard.columnId - 1;
            const editedId = editedCard.id;

            const editCardsColumn = editList[editColumn].todos.map((item) => {
                if (item.id === editedId) {
                    const editItem = {
                        ...item,
                        todotext: text,
                        level: numb,
                    };
                    return editItem;
                } else {
                    return item;
                }
            });
            editList[editColumn].todos = editCardsColumn;

            return {
                ...state,
                cardslist: editList,
            };

        case REMOVE_CARD:
            const slicePrevColumn = action.payload.prevColumn;
            const sliceId = action.payload.updatedCard.id;

            const sliceList = [...state.cardslist].map((column) => {
                if (column.id === slicePrevColumn) {
                    const sliceTodos = [...column.todos].filter(
                        (item) => item.id !== sliceId
                    );
                    const sliceColumn = { ...column, todos: sliceTodos };
                    return sliceColumn;
                } else {
                    return column;
                }
            });

            return {
                ...state,
                cardslist: sliceList,
            };

        case ADD_CARD:
            const addList = [...state.cardslist].map((column) => {
                if (column.id === action.payload.columnId) {
                    const addTodos = [...column.todos, action.payload];
                    const addColumn = { ...column, todos: addTodos };
                    return addColumn;
                } else {
                    return column;
                }
            });
            return {
                ...state,
                cardslist: addList,
            };

        default:
            return state;
    }
};
