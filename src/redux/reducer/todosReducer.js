import { ADD_NEW_TODO, CHANGE_TODO_STATUS, REMOVE_TODO } from "../constans";
import { deflist } from "./defaultdata";

export const initialState = {
    todolist: deflist,
};

export const mainReducer = (state, action) => {
    if (typeof state === "undefined") {
        return initialState;
    }

    switch (action.type) {
        case ADD_NEW_TODO:
            const addedList = [
                ...state.todolist,
                { ...action.payload, status: false },
            ];
            return {
                ...state,
                todolist: addedList,
            };

        case CHANGE_TODO_STATUS:
            const { id } = action.payload;
            const updatedList = state.todolist.map((item) => {
                if (item.id === id) {
                    const toggeItem = { ...item, status: !item.status };
                    return toggeItem;
                } else {
                    return item;
                }
            });
            return {
                ...state,
                todolist: updatedList,
            };

        case REMOVE_TODO:
            const idItem = action.payload.id;
            const shortList = state.todolist.filter(
                (item) => item.id !== idItem
            );
            return {
                ...state,
                todolist: shortList,
            };

        default:
            return state;
    }
};
