import uuid from "uuid/v4";
import { ADD_NEW_TODO } from "../constans";
import { ADD_COMMENT } from "../constans/commentsConstans";

export const generateIdToNewTodo = (store) => (next) => (action) => {
    if (action.type !== ADD_NEW_TODO) return next(action);

    const updatedAction = {
        ...action,
        payload: { ...action.payload, id: uuid() },
    };
    return next(updatedAction);
};

export const updateCommentId = (store) => (next) => (action) => {
    if (action.type !== ADD_COMMENT) return next(action);

    const { commentsList } = store.getState().comments;
    const currentId = commentsList[commentsList.length - 1].id + 1;
    const updatedAction = {
        ...action,
        payload: { ...action.payload, id: currentId },
    };
    return next(updatedAction);
};

export const exampleMiddleware = (store) => (next) => (action) => {
    // console.log("==================================");
    // console.log("action.type ", action.type);
    // console.log("action.payload ", action.payload);
    // console.log("store ", store.getState());
    // console.log("==================================");
    return next(action);
};
