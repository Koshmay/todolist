import {
    GET_TOTAL_CARDS,
    UPDATE_CARD_COLUMN,
    UPDATE_TASK_VALUES,
    GET_CARDS,
    EDIT_TASK,
    EDIT_VALUES,
    REMOVE_CARD,
    ADD_CARD,
} from "../constans";

export const putCardsCreator = (data) => {
    return {
        type: GET_TOTAL_CARDS,
        payload: data,
    };
};

export const updateCardColumnCreator = (updatedCard, prevColumn) => {
    return {
        type: UPDATE_CARD_COLUMN,
        payload: { updatedCard, prevColumn },
    };
};

export const editCardCreator = (updatedCard, text, numb) => {
    return {
        type: UPDATE_TASK_VALUES,
        payload: { updatedCard, text, numb },
    };
};

export const getTotalCardsCreator = () => {
    return {
        type: GET_CARDS,
    };
};

export const editCardValuesCreator = (data, valueText, valueLevel) => {
    return {
        type: EDIT_VALUES,
        payload: { data, valueText, valueLevel },
    };
};

export const dragCardCreator = (data, column) => {
    return {
        type: EDIT_TASK,
        payload: { data, column },
    };
};

export const removeCardColumnCreator = (updatedCard, prevColumn) => {
    return {
        type: REMOVE_CARD,
        payload: { updatedCard, prevColumn },
    };
};

export const addCardColumnCreator = (updatedCard) => {
    return {
        type: ADD_CARD,
        payload: updatedCard,
    };
};
