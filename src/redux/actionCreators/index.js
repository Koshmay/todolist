import { ADD_NEW_TODO, CHANGE_TODO_STATUS, REMOVE_TODO } from "../constans";

export const addTodoCreator = (data) => {
    return {
        type: ADD_NEW_TODO,
        payload: { text: data, id: null },
    };
};

export const changeTodoStatusCreator = (data) => {
    return {
        type: CHANGE_TODO_STATUS,
        payload: { id: data },
    };
};

export const removeTodoStatusCreator = (data) => {
    return {
        type: REMOVE_TODO,
        payload: { id: data },
    };
};
