import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import Layout from "./components/Layout";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import store from "./redux/store";

import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "semantic-ui-css/semantic.min.css";

import "./i18n";

const App = () => (
    <Provider store={store}>
        <Suspense fallback={null}>
            <Layout />
        </Suspense>
    </Provider>
);

ReactDOM.render(<App />, document.getElementById("root"));
serviceWorker.unregister();
