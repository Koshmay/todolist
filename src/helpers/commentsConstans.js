export const apiCommentsUrl = "https://jsonplaceholder.typicode.com/comments";

export const getPieData = (arr) => {
    // custom logic | get array of random numbers and remove some elements from current array
    const randomNumbers = Array.from({ length: 400 }, () =>
        Math.floor(Math.random() * 400)
    );
    randomNumbers.forEach((numb) => arr.splice(numb, 3.5));
    return arr;
};
