import uuid from "uuid/v4";

export const mockColumns = [
    { id: 1, title: "Doing" },
    { id: 2, title: "In progress" },
    { id: 3, title: "Waiting for MR" },
    { id: 4, title: "Ready for QA" },
    { id: 5, title: "Ready for deploy" },
];

export const mockTodos = [
    {
        todotext: "Buy pen2",
        id: uuid(),
        level: 1,
        columnId: 3,
    },
    {
        todotext: "Buy pensdfsdf2",
        id: uuid(),
        level: 1,
        columnId: 3,
    },
    {
        todotext: "Buy pendvdfvdfvd",
        id: uuid(),
        level: 1,
        columnId: 3,
    },
    {
        todotext: "gggg222",
        id: uuid(),
        level: 22225,
        columnId: 2,
    },
    {
        todotext: "Visit2",
        id: uuid(),
        level: 2,
        columnId: 4,
    },
    {
        todotext: "Visit",
        id: uuid(),
        level: 1,
        columnId: 2,
    },
    {
        todotext: "Do task 8",
        id: uuid(),
        level: 15,
        columnId: 1,
    },
    {
        todotext: "Do task 8   sdvsdvsdv",
        id: uuid(),
        level: 15,
        columnId: 1,
    },
    {
        todotext: "Do task 333",
        id: uuid(),
        level: 46,
        columnId: 1,
    },
    {
        todotext: "Do task 3",
        id: uuid(),
        level: 10,
        columnId: 4,
    },
    {
        todotext: "Do task 3 adasdasd",
        id: uuid(),
        level: 10,
        columnId: 4,
    },
    {
        todotext: "Do task 18",
        id: uuid(),
        level: 10,
        columnId: 4,
    },
];

export const mockData = [
    {
        id: 1,
        title: "Doing",
        todos: [
            { todotext: "Do task 8", id: uuid(), level: 15, columnId: 1 },
            { todotext: "Do task 333", id: uuid(), level: 46, columnId: 1 },
        ],
    },
    {
        id: 2,
        title: "In progress",
        todos: [
            { todotext: "gggg222", id: uuid(), level: 22225, columnId: 2 },
            { todotext: "Visit", id: uuid(), level: 1, columnId: 2 },
        ],
    },
    {
        id: 3,
        title: "Waiting for MR",
        todos: [{ todotext: "Buy pen2", id: uuid(), level: 1, columnId: 3 }],
    },
    {
        id: 4,
        title: "Ready for QA",
        todos: [
            { todotext: "Visit2", id: uuid(), level: 2, columnId: 4 },
            { todotext: "Do task 3", id: uuid(), level: 10, columnId: 4 },
        ],
    },
    { id: 5, title: "Ready for deploy", todos: [] },
];
