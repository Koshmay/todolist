import { css } from "@emotion/core";

export const apiKey = "SPboJP8XCDF9nlUzSqcqzh0Mq9sJuy6Hf27FuTFl"; // test
//export const apiKey = "nZIzRcVXxHYtO2JozbSvLwMkmyXHv1EsBNGZfaNL"; // my
export const apiUrl = "https://api.nasa.gov/neo/rest/v1/feed?start_date";
export const oneDayFormat = "DD";

export const override = css`
    display: block;
    margin: 0 auto;
`;

export const timescroll = 5000;
export const numberPoint = 3;
