import moment from "moment";

import { oneDayFormat, numberPoint } from "./nasaConstans";

export const getTodayDayNumber = () => {
    // return 25; // mockdata length
    return Number(moment().format(oneDayFormat));
};

export const getFormatNumber = (numb) => {
    return Number(numb).toFixed(numberPoint);
};

export const getDangerFlagColor = (totalNumbs, maxMumb, bigNumb) => {
    return (
        (maxMumb !== 0 && maxMumb === totalNumbs) ||
        (bigNumb !== 0 && bigNumb === totalNumbs)
    );
};

export const getItemData = (item) => {
    const dayTitle = item[0].close_approach_data[0].close_approach_date;

    let absM = 0,
        estimD = 0,
        dangers = 0,
        closeDist = 0,
        fastesSpeed = 0;

    item.forEach((arrayItem) => {
        const letAbsM = arrayItem.absolute_magnitude_h;
        const letEstimD =
            arrayItem.estimated_diameter.kilometers.estimated_diameter_max;
        const letDangers = arrayItem.is_potentially_hazardous_asteroid;
        const letCloseDist =
            arrayItem.close_approach_data[0].miss_distance.kilometers;
        const letFastesSpeed =
            arrayItem.close_approach_data[0].relative_velocity
                .kilometers_per_hour;

        if (absM < letAbsM) {
            absM = letAbsM;
        }
        if (estimD < letEstimD) {
            estimD = letEstimD;
        }
        if (letDangers) {
            dangers++;
        }
        if (closeDist < letCloseDist) {
            closeDist = letCloseDist;
        }
        if (fastesSpeed < letFastesSpeed) {
            fastesSpeed = letFastesSpeed;
        }
    });

    return {
        totalNumbs: dangers,
        maxEstimetedDiameter: getFormatNumber(estimD),
        closestDistance: getFormatNumber(closeDist),
        fastesNeo: getFormatNumber(fastesSpeed),
        dayTitle: dayTitle,
        absMagnitude: absM,
    };
};

export const getTimeStamp = (el) =>
    moment(el[0].close_approach_data[0].close_approach_date).format("X");

export const formatDate = (day) => {
    const currentYearMonth = moment().format("YYYY-MM-");

    return currentYearMonth + (day > 9 ? day : `0${day}`);
};

export const getDangerLevels = (tempArr) => {
    let dangerousPoints = [];
    for (let i = 0; i < tempArr.length; i++) {
        let dangerCount = 0;
        for (let j = 0; j < tempArr[i].length; j++) {
            if (tempArr[i][j].is_potentially_hazardous_asteroid) {
                dangerCount++;
            }
        }
        dangerousPoints.push(dangerCount);
    }

    const [theMostDanger, theDanger] = dangerousPoints.sort((a, b) => b - a);

    return {
        theMostDanger,
        theDanger,
    };
};

export const getMaxDayNumber = () => {
    const today = getTodayDayNumber();

    if (today < 6) {
        return today;
    } else {
        return 6;
    }
};
