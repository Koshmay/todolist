export const getWindowSize = () => {
    return typeof window !== "undefined" && window.innerWidth >= 960;
};
