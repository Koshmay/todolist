import React from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import EditIcon from "@material-ui/icons/Edit";
import Button from "../../Common/Button";
import { setUpdateComment } from "../../../redux/actions/commentsActions";

import "./index.css";

const MediaCard = ({ data, setUpdateComment }) => {
    return (
        <div className="mediacard">
            <div>
                <h3 className="mediacard__title">
                    <span className="mediacard__circle">#{data.id}</span>
                    {data.name}
                </h3>
                <p
                    className={classnames("mediacard__text", {
                        "mediacard__text--updated": data.updated,
                    })}
                >
                    {data.body}
                </p>
            </div>
            <Button variant="success" onClick={() => setUpdateComment(data)}>
                <EditIcon color="action" />
            </Button>
        </div>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUpdateComment: (data) => dispatch(setUpdateComment(data)),
    };
};

export default connect(null, mapDispatchToProps)(MediaCard);
