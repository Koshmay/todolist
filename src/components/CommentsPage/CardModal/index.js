import React, { useEffect, useState } from "react";
import Modal from "react-modal";
import { connect } from "react-redux";
import classnames from "classnames";
import BlockIcon from "@material-ui/icons/Block";
import BorderColorIcon from "@material-ui/icons/BorderColor";
import {
    removeComment,
    skipDataComment,
    addComment,
} from "../../../redux/actions/commentsActions";
import {
    getOpenCommentsModal,
    getUpdatedDataModal,
} from "../../../redux/selectors/commentsSelector";
import "./index.css";

const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
    },
};

const CardModal = ({ data, open, remove, skip, add }) => {
    Modal.setAppElement("#root");

    const [showCardModal, setShowCardModal] = useState(false);
    const [editData, setEditData] = useState({});
    const [emptyValue, setEmptyValue] = useState(false);

    const closeModal = () => {
        skip();
        setEmptyValue(false);
    };

    const changeText = (e) => {
        const data = { ...editData };
        const { value } = e.target;
        data.body = value;
        setEditData(data);

        if (value.trim() === "") {
            setEmptyValue(true);
        }
        if (value.trim() !== "" && emptyValue) {
            setEmptyValue(false);
        }
    };

    const updateData = () => {
        if (!emptyValue) {
            remove(editData.id);
            const updateComment = { ...editData, updated: 1 };
            add(updateComment);
            skip();
        }
    };

    useEffect(() => {
        setEditData(data);
        setShowCardModal(open);
    }, [data, open]);

    return (
        <Modal
            isOpen={showCardModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Modal"
        >
            <div className="modal-buttons">
                <BlockIcon
                    color="action"
                    className="modal__icon"
                    onClick={closeModal}
                />
                <BorderColorIcon
                    color="action"
                    className={classnames("modal__icon", {
                        modal__icon__skip: emptyValue,
                    })}
                    onClick={updateData}
                />
            </div>
            <form>
                <input
                    type="text"
                    value={editData.email || ""}
                    className="modal__input"
                    disabled
                />
                <input
                    type="name"
                    value={editData.name || ""}
                    className="modal__input"
                    disabled
                />
                <textarea
                    rows="4"
                    cols="50"
                    value={editData.body || ""}
                    className={classnames("modal__input", {
                        modal__input__empty: emptyValue,
                    })}
                    onChange={changeText}
                />
            </form>
        </Modal>
    );
};

const mapStateToProps = (state) => {
    return {
        data: getUpdatedDataModal(state),
        open: getOpenCommentsModal(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        remove: (data) => dispatch(removeComment(data)),
        add: (data) => dispatch(addComment(data)),
        skip: () => dispatch(skipDataComment()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CardModal);
