import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchComments } from "../../../redux/actions/commentsActions";
import {
    getCommentsList,
    getCommentsLoading,
} from "../../../redux/selectors/commentsSelector";
import Loader from "../../Common/Loader";
import MediaCard from "../MediaCard";
import CardModal from "../CardModal";
import Navigation from "../../Common/Navigation";

import "./index.css";

const CommentsContainer = ({ getComments, list, loading }) => {
    useEffect(() => {
        getComments();
    }, [getComments]);

    if (loading) return <Loader />;

    return (
        <div className="comments">
            <Navigation />
            <div className="comments-container">
                {list.length > 0 &&
                    list.map((item) => <MediaCard key={item.id} data={item} />)}
                <CardModal />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        list: getCommentsList(state),
        loading: getCommentsLoading(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getComments: () => dispatch(fetchComments()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentsContainer);
