import React from "react";
import classnames from "classnames";
import "./index.css";

const Input = ({ className, ...rest }) => {
    return (
        <input
            {...rest}
            className={classnames("input", className)}
            type={`${rest.type || "text"}`}
            placeholder=" "
        />
    );
};

export default Input;
