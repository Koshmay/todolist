import React from "react";
import { Link } from "react-router-dom";
import { layuotLinks } from "../../../Layout/layoutLinks";
import "./index.css";

const MainMobBlock = () => {
    return (
        <div className="main-mob-block">
            <Link className="main-mob-block-link" to={layuotLinks.nasa.to}>
                {layuotLinks.nasa.title}
            </Link>
            <Link
                className="main-mob-block-link main-mob-block-link--center"
                to={layuotLinks.comments.to}
            >
                {layuotLinks.comments.title}
            </Link>
            <Link
                className="main-mob-block-link main-mob-block-link--hidden"
                to={layuotLinks.dnd.to}
            >
                {layuotLinks.dnd.title}
            </Link>
            <Link className="main-mob-block-link" to={layuotLinks.currency.to}>
                {layuotLinks.currency.title}
            </Link>
        </div>
    );
};

export default MainMobBlock;
