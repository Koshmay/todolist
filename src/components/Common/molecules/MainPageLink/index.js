import React from "react";
import classnames from "classnames";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

const MainPageLink = ({
    title,
    className,
    url,
    src,
    time = 400,
    onClick,
    ...rest
}) => {
    const history = useHistory();

    const onLinkOpen = (e) => {
        e.preventDefault();
        setTimeout(() => {
            history.push(url);
            // fix
            if (url === "/") {
                console.log("window.location.reload()");
                window.location.reload();
            }
        }, time);
        if (onClick) {
            onClick();
        }
    };

    return (
        <Link
            {...rest}
            onClick={onLinkOpen}
            className={classnames("main-page-link", className)}
            to={url}
        >
            <div className="main-page-link__title">{title}</div>
        </Link>
    );
};

export default MainPageLink;
