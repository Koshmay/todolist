import React, { Component } from "react";
import "./index.css";

const movingCursorId = [
    "moving--cursor__item--A",
    "moving--cursor__item--B",
    "moving--cursor__item--C",
    "moving--cursor__item--D",
    "moving--cursor__item--E",
    "moving--cursor__item--F",
    "moving--cursor__item--G",
];
class MovingCursor extends Component {
    trialMouse = (e, itemA, itemB, itemC, itemD, itemE, itemF, itemG) => {
        let $mouseX = e.pageX,
            $mouseY = e.pageY,
            $xp = 0,
            $yp = 0;

        $xp += $mouseX - $xp;
        $yp += $mouseY - $yp;

        const xw = $xp - 12 + "px";
        const yw = $yp - 12 + "px";

        requestAnimationFrame(() => {
            itemA.style.left = xw;
            itemA.style.top = yw;
            itemB.style.left = xw;
            itemB.style.top = yw;
            itemC.style.left = xw;
            itemC.style.top = yw;
            itemD.style.left = xw;
            itemD.style.top = yw;
            itemE.style.left = xw;
            itemE.style.top = yw;

            itemF.style.left = xw;
            itemF.style.top = yw;

            itemG.style.left = xw;
            itemG.style.top = yw;
        });
    };

    componentDidMount() {
        const itemA = document.getElementById("moving--cursor__item--A");
        const itemB = document.getElementById("moving--cursor__item--B");
        const itemC = document.getElementById("moving--cursor__item--C");
        const itemD = document.getElementById("moving--cursor__item--D");
        const itemE = document.getElementById("moving--cursor__item--E");
        const itemF = document.getElementById("moving--cursor__item--F");
        const itemG = document.getElementById("moving--cursor__item--G");

        document.addEventListener("mousemove", (e) =>
            this.trialMouse(e, itemA, itemB, itemC, itemD, itemE, itemF, itemG)
        );
    }

    componentWillUnmount() {
        document.removeEventListener("mousemove", (e) => this.trialMouse(e));
    }

    render() {
        return (
            <section className="moving-cursor__block">
                {movingCursorId.map((id, index) => (
                    <section
                        key={index}
                        className="moving--cursor__item"
                        id={id}
                    />
                ))}
            </section>
        );
    }
}

export default MovingCursor;
