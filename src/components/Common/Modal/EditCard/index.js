import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "react-bootstrap/Modal";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "../../Button";
import { editCardValuesCreator } from "../../../../redux/actionCreators/cardsCreators";

const ModalWindow = ({ isShowing, setShow, data }) => {
    const { todotext, level } = data;

    const [valueText, setValueText] = useState(todotext);
    const [valueLevel, setValueLevel] = useState(level);
    const dispatch = useDispatch();

    const handleChangeText = (e) => {
        setValueText(e.target.value);
    };

    const handleChangeLevel = (e) => {
        setValueLevel(Number(e.target.value));
    };

    const handleSubmit = () => {
        if (valueText && valueText.trim() !== "" && Number(valueLevel)) {
            dispatch(editCardValuesCreator(data, valueText, valueLevel));
            setShow(false);
        }
    };

    return (
        <Modal
            show={isShowing}
            onHide={() => setShow(false)}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
        >
            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">
                    <InputGroup>
                        <FormControl
                            placeholder={valueText}
                            aria-label="card value"
                            aria-describedby="basic-addon2"
                            onChange={handleChangeText}
                            value={valueText}
                            isInvalid={valueText.trim() === ""}
                        />
                        <FormControl
                            type="number"
                            placeholder={valueLevel}
                            aria-label="card level"
                            aria-describedby="basic-addon2"
                            onChange={handleChangeLevel}
                            value={valueLevel}
                            isInvalid={Number(valueLevel) < 1}
                            min={1}
                        />
                        <InputGroup.Append>
                            <Button variant="warning" onClick={handleSubmit}>
                                Edit
                            </Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Modal.Title>
            </Modal.Header>
        </Modal>
    );
};

export default ModalWindow;
