import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Modal from "react-bootstrap/Modal";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import uuid from "uuid/v4";
import Button from "../../Button";
import { addCardColumnCreator } from "../../../../redux/actionCreators/cardsCreators";

const ModalAddCard = ({ isShowing, closeModal, customId }) => {
    const [valueText, setValueText] = useState("");
    const [valueLevel, setValueLevel] = useState(1);

    const dispatch = useDispatch();

    const handleChangeText = (e) => {
        setValueText(e.target.value);
    };

    const handleChangeLevel = (e) => {
        setValueLevel(Number(e.target.value));
    };

    const handelClose = () => {
        setValueText("");
        setValueLevel(1);
        closeModal();
    };

    const handleSubmit = () => {
        if (valueText && valueText.trim() !== "" && Number(valueLevel)) {
            const newCard = {
                columnId: customId,
                todotext: valueText,
                level: valueLevel,
                id: uuid(),
            };
            dispatch(addCardColumnCreator(newCard));
            handelClose();
        } else {
            handelClose();
        }
    };

    return (
        <Modal
            show={isShowing}
            onHide={handelClose}
            dialogClassName="modal-120w"
            aria-labelledby="example-custom-modal-styling-title"
        >
            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="text">Text</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            placeholder={valueText}
                            aria-label="card value"
                            aria-describedby="basic-addon2"
                            onChange={handleChangeText}
                            value={valueText}
                            isInvalid={valueText.trim() === ""}
                            as="textarea"
                        />
                    </InputGroup>

                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="level">Level</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            type="number"
                            placeholder={valueLevel}
                            aria-label="card level"
                            aria-describedby="basic-addon2"
                            onChange={handleChangeLevel}
                            value={valueLevel}
                            isInvalid={Number(valueLevel) < 1}
                            min={1}
                        />
                    </InputGroup>

                    <InputGroup>
                        <Button variant="warning" onClick={handleSubmit} block>
                            Add
                        </Button>
                    </InputGroup>
                </Modal.Title>
            </Modal.Header>
        </Modal>
    );
};

export default ModalAddCard;
