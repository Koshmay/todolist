import React from "react";
import { Link } from "react-router-dom";
import { layuotLinks } from "../../Layout/layoutLinks";
import "./index.css";

const Navigation = () => {
    return (
        <div className="navigation">
            <Link to="/" className="link link--navigation">
                Index
            </Link>
            <Link
                to={layuotLinks.dnd.to}
                className="link link--navigation link--board"
            >
                {layuotLinks.dnd.title}
            </Link>
            <Link
                to={layuotLinks.nasa.to}
                className="link link--navigation link--nasa"
            >
                {layuotLinks.nasa.title}
            </Link>
            <Link
                to={layuotLinks.comments.to}
                className="link link--navigation link--comments"
            >
                {layuotLinks.comments.title}
            </Link>
            <Link
                to={layuotLinks.currency.to}
                className="link link--navigation link--currency"
            >
                {layuotLinks.currency.title}
            </Link>
        </div>
    );
};

export default Navigation;
