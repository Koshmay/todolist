import React from "react";
import { FadeLoader } from "react-spinners";
import { override } from "../../../helpers/nasaConstans";

const Loader = () => {
    return <FadeLoader css={override} color={"#1e90ff"} />;
};

export default Loader;
