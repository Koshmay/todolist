import React from "react";
import classnames from "classnames";
import { Button as Btn } from "react-bootstrap";
import "./index.css";

const Button = ({ children, className, variant = "secondary", ...rest }) => (
    <Btn
        {...rest}
        variant={variant}
        type="button"
        className={classnames(className, "button")}
    >
        {children}
    </Btn>
);

export default Button;
