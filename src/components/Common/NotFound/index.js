import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
    return (
        <div className="not-found">
            <h1>Page not found</h1>
            <Link to="/">Go to index page</Link>
        </div>
    );
};

export default NotFound;
