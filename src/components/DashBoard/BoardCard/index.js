import React, { useState, useEffect, Fragment } from "react";
import { useDispatch } from "react-redux";
import { useDrag } from "react-dnd";
import { get } from "lodash";
import EditIcon from "@material-ui/icons/Edit";
import { getEmptyImage } from "react-dnd-html5-backend";
import ModalWindow from "../../Common/Modal/EditCard";
import { dragCardCreator } from "../../../redux/actionCreators/cardsCreators";
import DragLayerComponent from "../DragLayer";

import "./index.css";

const BoardCard = ({ data }) => {
    const [show, setShow] = useState(false);
    const [isDragging, setIsDragging] = useState(false);
    const dispatch = useDispatch();

    // eslint-disable-next-line
    const [collectedProps, drag, preview] = useDrag({
        item: {
            ...data,
            type: "card",
        },
        begin: () => {
            setIsDragging(true);
        },
        end: (item, monitor) => {
            const result = monitor.getDropResult();
            const newColumnId = get(result, "columnId") || item.columnId;

            if (newColumnId !== item.columnId) {
                dispatch(dragCardCreator(data, newColumnId));
            }
            setIsDragging(false);
        },
    });

    useEffect(() => {
        preview(getEmptyImage(), { captureDraggingState: true });
        // eslint-disable-next-line
    }, []);

    if (!data) return null;

    return (
        <Fragment>
            <DragLayerComponent draggbleId={data.id} isDrag={isDragging} />
            <div className="board-card" ref={drag}>
                <div className="board-card__title">
                    <span className="board-card__title--text">
                        {data.todotext}
                    </span>
                    <EditIcon
                        color="action"
                        className="board-card__icon"
                        onClick={() => setShow(true)}
                    />
                </div>
                <div className="board-card__level">Level {data.level}</div>
            </div>
            <ModalWindow isShowing={show} setShow={setShow} data={data} />
        </Fragment>
    );
};

export default BoardCard;
