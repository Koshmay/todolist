import React from "react";
import classnames from "classnames";
import { useDrop } from "react-dnd";
import BoardCard from "../BoardCard";

import "./index.css";

const TodosBoardContainer = ({ todos = [], id }) => {
    // eslint-disable-next-line
    const [collectedProps, drop] = useDrop({
        accept: "card",
        drop: () => ({ columnId: id, todos }),
    });

    return (
        <div
            className={classnames(
                "todos-board-container",
                `todos-board-container--${id}`
            )}
            ref={drop}
        >
            {todos.map((todo) => (
                <BoardCard key={todo.id} data={todo} />
            ))}
        </div>
    );
};
export default TodosBoardContainer;
