import React from "react";
import HTML5Backend from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";
import Board from "../Board";

const BoardDnDWrapper = () => {
    return (
        <DndProvider backend={HTML5Backend}>
            <Board />
        </DndProvider>
    );
};

export default BoardDnDWrapper;
