import React, { useState } from "react";
import { useSelector } from "react-redux";
import Spinner from "react-bootstrap/Spinner";
import AddAlarmIcon from "@material-ui/icons/AddAlarm";
import classnames from "classnames";
import TodosBoardContainer from "../TodosBoardContainer";
import ModalAddCard from "../../Common/Modal/AddCard";
import "./index.css";

const BoardContainer = () => {
    const [show, setShow] = useState(false);
    const [customId, setCustomId] = useState(null);

    const totalTodos = useSelector((state) => state.cards.cardslist);

    const openModal = (id) => {
        setCustomId(id);
        setShow(true);
    };
    const closeModal = () => {
        setCustomId(null);
        setShow(false);
    };
    if (!totalTodos.length || totalTodos === undefined) {
        return <Spinner animation="border" />;
    }

    return (
        <div className="board-container">
            {totalTodos.length &&
                totalTodos.map((item) => (
                    <div key={item.id} className="board-container__column">
                        <div className="board-container__thead">
                            {item.title}
                            <AddAlarmIcon
                                color="action"
                                fontSize="large"
                                className={classnames(
                                    "board-container__icon",
                                    `board-container__icon--${item.id}`
                                )}
                                onClick={() => openModal(item.id)}
                            />
                        </div>
                        <TodosBoardContainer todos={item.todos} id={item.id} />
                        <ModalAddCard
                            isShowing={show}
                            closeModal={closeModal}
                            customId={customId}
                        />
                    </div>
                ))}
        </div>
    );
};
export default BoardContainer;
