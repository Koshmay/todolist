import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Navigation from "../../Common/Navigation";
import BoardContainer from "../BoardContainer";
import { getTotalCardsCreator } from "../../../redux/actionCreators/cardsCreators";

import "./index.css";

const Board = () => {
    const dispatch = useDispatch();
    const totalTodos = useSelector((state) => state.cards.cardslist);

    const getData = () => {
        if (totalTodos.length) return;
        dispatch(getTotalCardsCreator());
    };

    useEffect(() => {
        getData();
        // eslint-disable-next-line
    }, []);

    return (
        <div className="board">
            <Navigation />
            <BoardContainer />
        </div>
    );
};

export default Board;
