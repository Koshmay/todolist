import React from "react";
import { get } from "lodash";
import classnames from "classnames";

import { useDragLayer } from "react-dnd";

const layerStyles = {
    position: "fixed",
    pointerEvents: "none",
    zIndex: 100,
    left: 0,
    top: 0,
};

function snapToGrid(x, y) {
    const snappedX = Math.round(x / 32) * 32;
    const snappedY = Math.round(y / 32) * 32;
    return [snappedX, snappedY];
}

function getItemStyles(initialOffset, currentOffset, isSnapToGrid) {
    if (!initialOffset || !currentOffset) {
        return {
            display: "none",
        };
    }
    let { x, y } = currentOffset;
    if (isSnapToGrid) {
        x -= initialOffset.x;
        y -= initialOffset.y;
        [x, y] = snapToGrid(x, y);
        x += initialOffset.x;
        y += initialOffset.y;
    }
    const transform = `translate(${x}px, ${y}px)`;
    return {
        transform,
        WebkitTransform: transform,
    };
}

const DragLayerComponent = (props) => {
    const { item, initialOffset, currentOffset } = useDragLayer((monitor) => ({
        item: monitor.getItem(),
        initialOffset: monitor.getInitialSourceClientOffset(),
        currentOffset: monitor.getSourceClientOffset(),
    }));

    const cardId = get(item, "id");

    return (
        <div style={layerStyles}>
            {props.draggbleId === cardId && (
                <div
                    style={getItemStyles(
                        initialOffset,
                        currentOffset,
                        props.snapToGrid
                    )}
                >
                    <div
                        className={classnames("board-card", {
                            "board-card__drugging": props.isDrag,
                        })}
                    >
                        <div className="board-card__title">{item.todotext}</div>
                        <div className="board-card__level">
                            Level {item.level}
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};
export default DragLayerComponent;
