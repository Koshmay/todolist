import React, { useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { connect } from "react-redux";
import NeoListContainer from "../NeoListContainer/index";
import { fetchNeos } from "../../../redux/actions/nasaActions";
import { getCountNeos } from "../../../redux/selectors/nasaSelectors";
import Navigation from "../../Common/Navigation";
import "./index.css";

const Container = ({ fetchNeosAction, count }) => {
    useEffect(() => {
        fetchNeosAction();
    }, [fetchNeosAction]);

    const getCountNumb = (numb) => {
        return numb > 1 ? numb - 1 : 0;
    };

    return (
        <div className={classnames("nasa-container")}>
            <Navigation />
            <h1 className={classnames("nasa-container__header")}>
                NEAR ORBITAL OBJECTS
            </h1>
            <h6 className="nasa-container_footer">
                Updated {getCountNumb(count)} times
            </h6>
            <NeoListContainer />
        </div>
    );
};

Container.propTypes = {
    fetchNeosAction: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    count: getCountNeos(state),
});

export default connect(mapStateToProps, { fetchNeosAction: fetchNeos })(
    Container
);
