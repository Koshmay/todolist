import React from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import List from "@material-ui/core/List";
import NeosItem from "../NeosItem/index";
import {
    getDangerPoints,
    getTempNeos,
} from "../../../redux/selectors/nasaSelectors";
import { getItemData, getDangerFlagColor } from "../../../helpers/nasaHelpers";
import "./index.css";
import { makeStyles } from "@material-ui/core/styles";

export const useListStyles = makeStyles({
    root: {
        width: "60%",
        textAlign: "left",
        margin: "2% 0",
    },
    inline: {
        display: "inline",
    },
});

const NeosList = ({
    showedDays,
    dangerLevels: { theMostDanger, theDanger },
}) => {
    const listClasses = useListStyles();

    return (
        <div className="neolist__container">
            <List className={classnames("neolist__list", listClasses.root)}>
                {showedDays.map((item, index) => {
                    const {
                        totalNumbs,
                        dayTitle,
                        absMagnitude,
                        maxEstimetedDiameter,
                        closestDistance,
                        fastesNeo,
                    } = getItemData(item);

                    const dangerFlagColor = getDangerFlagColor(
                        totalNumbs,
                        theMostDanger,
                        theDanger
                    );

                    return (
                        <li key={index}>
                            <NeosItem
                                totalNumbs={totalNumbs}
                                dayTitle={dayTitle}
                                absMagnitude={absMagnitude}
                                maxEstimetedDiameter={maxEstimetedDiameter}
                                closestDistance={closestDistance}
                                fastesNeo={fastesNeo}
                                dangerFlagColor={dangerFlagColor}
                            />
                        </li>
                    );
                })}
            </List>
        </div>
    );
};

const mapStateToProps = (state) => ({
    showedDays: getTempNeos(state),
    dangerLevels: getDangerPoints(state),
});

export default connect(mapStateToProps)(NeosList);
