import React, { Component } from "react";
import Loader from "../../Common/Loader";
import { connect } from "react-redux";
import NeosList from "../NeosList/index";
import { getTempNeos, getError } from "../../../redux/selectors/nasaSelectors";
import "./index.css";

class NeoListContainer extends Component {
    render() {
        const { isErrorRequest = false, piesData = [] } = this.props;
        if (isErrorRequest) {
            return (
                <div className="neo-list-loader">
                    <h2 className="neo-list-loader__title--error">
                        FAILED REQUEST
                    </h2>
                    <h2 className="neo-list-loader__title--error">
                        TRY NEXT DAY
                    </h2>
                </div>
            );
        }

        if (!piesData.length) {
            return (
                <div className="neo-list-loader">
                    <Loader />
                    <h2 className="neo-list-loader__title">Fetching data</h2>
                </div>
            );
        }

        return <NeosList />;
    }
}

const mapStateToProps = (state) => {
    return {
        isErrorRequest: getError(state),
        piesData: getTempNeos(state),
    };
};

export default connect(mapStateToProps)(NeoListContainer);
