import React from "react";
import NumberFormat from "react-number-format";
import classnames from "classnames";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import "./index.css";

import { makeStyles } from "@material-ui/core/styles";

export const usePaperStyles = makeStyles({
    root: {
        padding: "4px 10%",
        marginTop: "12px",
        backgroundColor: "#E0FFFF",
    },
});

const NeosItem = ({
    totalNumbs,
    dayTitle,
    absMagnitude,
    maxEstimetedDiameter,
    closestDistance,
    fastesNeo,
    dangerFlagColor,
}) => {
    const classesPaper = usePaperStyles();

    return (
        <div className="neo-item">
            <Paper
                className={classnames(classesPaper.root, {
                    "neo-item__paper--danger": dangerFlagColor,
                })}
            >
                <div className="neo-item__title">Day {dayTitle}</div>
                <CardContent>
                    <Typography component="div" className="neo-item__row">
                        <span>Max estimater diameter</span>
                        <span className="neo-item__cell--marked">
                            {maxEstimetedDiameter} <span>km</span>
                        </span>
                    </Typography>

                    <Typography component="div" className="neo-item__row">
                        <span>Potentially hazardous per day </span>
                        <span className="neo-item__cell--marked">
                            {totalNumbs}
                        </span>
                    </Typography>

                    <Typography component="div" className="neo-item__row">
                        <span>Closest NEO distance</span>
                        <span className="neo-item__cell--marked">
                            <NumberFormat
                                value={closestDistance}
                                displayType={"text"}
                                thousandSeparator={true}
                                renderText={(value) => <span>{value}</span>}
                            />{" "}
                            km
                        </span>
                    </Typography>

                    <Typography component="div" className="neo-item__row">
                        <span>The fastes NEO</span>
                        <span className="neo-item__cell--marked">
                            <NumberFormat
                                value={fastesNeo}
                                displayType={"text"}
                                thousandSeparator={true}
                                renderText={(value) => <span>{value}</span>}
                            />{" "}
                            km/h
                        </span>
                    </Typography>

                    <Typography component="div" className="neo-item__row">
                        <span>Absolute magnitude</span>
                        <span className="neo-item__cell--marked">
                            {absMagnitude} H
                        </span>
                    </Typography>
                </CardContent>
            </Paper>
        </div>
    );
};

export default NeosItem;
