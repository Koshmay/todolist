import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const a11yProps = (index) => {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
};

const CurrencyBar = ({ handleChangeTabs, activeTab }) => {
    return (
        <AppBar position="static">
            <Tabs
                value={activeTab}
                onChange={handleChangeTabs}
                aria-label="currency tabs"
            >
                <Tab label="Choose Schema" {...a11yProps(0)} />
                <Tab label="Choose Language" {...a11yProps(1)} />
                <Tab label="Choose Currency" {...a11yProps(2)} />
            </Tabs>
        </AppBar>
    );
};

export default CurrencyBar;
