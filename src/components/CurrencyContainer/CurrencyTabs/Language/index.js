import React from "react";
import { connect } from "react-redux";

import { useTranslation } from "react-i18next";
import { FormGroup, Label, Input } from "reactstrap";

import { getLanguage } from "../../../../redux/selectors/currencySelector";
import { setAppLanguage } from "../../../../redux/actions/currencyActions";

import TabPanel from "../TabPanel";
import { texts } from "../../../../helpers/i18nStrings";

const Language = ({ activeTab, lang, translate }) => {
    const { t, i18n } = useTranslation();

    const changeLanguage = (event) => {
        translate(event.target.value);
        i18n.changeLanguage(event.target.value);
    };

    if (activeTab !== 1) return null;

    return (
        <TabPanel value={activeTab} index={1} className="currency--panel-item">
            <FormGroup>
                <p>
                    <Label for="language">{t(texts.language)}</Label>
                </p>
                <Input
                    type="select"
                    name="language"
                    id="language"
                    onChange={changeLanguage}
                    defaultValue={lang}
                >
                    <option value="en" name="language">
                        English
                    </option>
                    <option value="uk" name="language">
                        Ukrainian
                    </option>
                    <option value="ru" name="language">
                        Russian
                    </option>
                </Input>
            </FormGroup>

            <br />
            <hr />
            <br />
            <p>{t(texts.hello)}</p>
            <p>{t(texts.exampleText)}</p>
        </TabPanel>
    );
};

const mapStateToProps = (state) => {
    return {
        lang: getLanguage(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        translate: (value) => dispatch(setAppLanguage(value)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Language);
