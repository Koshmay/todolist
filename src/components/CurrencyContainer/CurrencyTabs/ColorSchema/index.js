import React from "react";
import { Form, Checkbox } from "semantic-ui-react";
import TabPanel from "../TabPanel";

const ColorSchema = ({ activeTab, handelSchema, isChecked }) => {
    if (activeTab !== 0) return null;

    return (
        <TabPanel value={activeTab} index={0} className="currency--panel-item">
            <Form>
                <Form.Field>
                    <Checkbox
                        toggle
                        label="Schema Light / Dark"
                        onChange={handelSchema}
                        checked={isChecked}
                    />
                </Form.Field>
            </Form>
        </TabPanel>
    );
};

export default ColorSchema;
