import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import Navigation from "../../Common/Navigation";
import { makeStyles } from "@material-ui/core/styles";
import { getSchema } from "../../../redux/selectors/currencySelector";
import { setSchemaAction } from "../../../redux/actions/currencyActions";

import CurrencyBar from "../CurrencyTabs/CurrencyBar";
import ColorSchema from "../CurrencyTabs/ColorSchema";
import Language from "../CurrencyTabs/Language";
import TabPanel from "../CurrencyTabs/TabPanel";

import "./index.css";
import "./currency_material.css";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

const Currency = ({ schema, setSchema }) => {
    const [darkSchema, setDarkSchema] = useState(false);
    const [activeTab, setActiveTab] = useState(0);

    const handleChangeTabs = (event, newValue) => {
        setActiveTab(newValue);
    };

    const handelSchema = (event, newValue) => {
        setSchema(Boolean(newValue.checked));
    };
    const classes = useStyles();

    useEffect(() => {
        setDarkSchema(schema);
    }, [schema]);

    return (
        <div
            className={classnames("currency", { "currency--dark": darkSchema })}
        >
            <Navigation />
            <section className={classes.root}>
                <CurrencyBar
                    handleChangeTabs={handleChangeTabs}
                    activeTab={activeTab}
                />
                <ColorSchema
                    handelSchema={handelSchema}
                    activeTab={activeTab}
                    isChecked={darkSchema}
                />
                <Language activeTab={activeTab} />
                <TabPanel
                    value={activeTab}
                    index={2}
                    className="currency--panel-item"
                >
                    Choose Currency
                </TabPanel>
            </section>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        schema: getSchema(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setSchema: (value) => dispatch(setSchemaAction(value)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Currency);
