export const layuotLinks = {
    nasa: {
        to: "/nasa",
        title: "Nasa List",
    },
    dnd: {
        to: "/dnd",
        title: "D-N-D",
    },
    comments: {
        to: "/comments",
        title: "Comments",
    },
    currency: {
        to: "/currency",
        title: "Currency",
    },
};
