import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import IndexPage from "../IndexPage";
// import MainContainer from "../SimpleTodos/MainContainer";
import BoardDnDWrapper from "../DashBoard/BoardDnDWrapper";
import CommentsContainer from "../CommentsPage/Container";
import Currency from "../CurrencyContainer/Currency";
import NotFound from "../Common/NotFound";
import { layuotLinks } from "./layoutLinks";
import "./index.css";

import Container from "../Nasa/NasaContainer";

const Layout = () => {
    return (
        <Router>
            <Switch>
                <Route path={layuotLinks.dnd.to}>
                    <BoardDnDWrapper />
                </Route>
                <Route path={layuotLinks.nasa.to}>
                    <Container />
                </Route>
                <Route path={layuotLinks.comments.to}>
                    <CommentsContainer />
                </Route>
                <Route path={layuotLinks.currency.to}>
                    <Currency />
                </Route>
                <Route exact path="/">
                    <IndexPage />
                </Route>
                <Route path="*">
                    <NotFound />
                </Route>
            </Switch>
        </Router>
    );
};

export default Layout;
