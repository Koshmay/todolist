import React from "react";
import MainPageLink from "../Common/molecules/MainPageLink";
import IndexPageSircles from "./circles";
import { layuotLinks } from "../Layout/layoutLinks";

const IndexPageBlock = ({
    openLink,
    moveCustomCircle,
    removeInlineStyles,
    isLinkOpened,
}) => {
    return (
        <div className="index-page__block">
            <MainPageLink
                title={layuotLinks.nasa.title}
                className="main-page__title main-page__title--left"
                url={layuotLinks.nasa.to}
                onClick={openLink}
                onMouseMove={(e) =>
                    moveCustomCircle(
                        e,
                        "index-page__small-circle--left",
                        "index-page__big-circle--left"
                    )
                }
                onMouseLeave={() => {
                    removeInlineStyles(
                        "index-page__small-circle--left",
                        "index-page__big-circle--left"
                    );
                }}
            />
            <MainPageLink
                title={layuotLinks.dnd.title}
                className="main-page__title main-page__title--right"
                url={layuotLinks.dnd.to}
                onClick={openLink}
                onMouseMove={(e) =>
                    moveCustomCircle(
                        e,
                        "index-page__small-circle--right",
                        "index-page__big-circle--right"
                    )
                }
                onMouseLeave={() => {
                    removeInlineStyles(
                        "index-page__small-circle--right",
                        "index-page__big-circle--right"
                    );
                }}
            />
            <MainPageLink
                title={layuotLinks.comments.title}
                className="main-page__title main-page__title--top"
                url={layuotLinks.comments.to}
                onClick={openLink}
                onMouseMove={(e) =>
                    moveCustomCircle(
                        e,
                        "index-page__small-circle--top",
                        "index-page__big-circle--top"
                    )
                }
                onMouseLeave={() => {
                    removeInlineStyles(
                        "index-page__small-circle--top",
                        "index-page__big-circle--top"
                    );
                }}
            />
            <MainPageLink
                title={layuotLinks.currency.title}
                className="main-page__title main-page__title--bottom"
                url={layuotLinks.currency.to}
                onClick={openLink}
                onMouseMove={(e) =>
                    moveCustomCircle(
                        e,
                        "index-page__small-circle--bottom",
                        "index-page__big-circle--bottom"
                    )
                }
                onMouseLeave={() => {
                    removeInlineStyles(
                        "index-page__small-circle--bottom",
                        "index-page__big-circle--bottom"
                    );
                }}
            />
            <IndexPageSircles isLinkOpened={isLinkOpened} />
        </div>
    );
};

export default IndexPageBlock;
