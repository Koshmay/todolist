import React, { useState } from "react";
import MainMobBlock from "../Common/molecules/MainMobBlock";
import MovingCursor from "../Common/atoms/MovingCursor";
import IndexPageBlock from "./block";
import { getWindowSize } from "../../helpers";

import "./index.css";

const IndexPage = () => {
    const [isLinkOpened, setLinkOpened] = useState(false);

    const openLink = () => {
        setLinkOpened(true);
    };

    const moveCustomCircle = (e, classSmallCircle, classBigCircle) => {
        if (!getWindowSize()) {
            return;
        }

        if (classSmallCircle && classBigCircle) {
            removeInlineStyles(classSmallCircle, classBigCircle);

            const itemSmall = document.getElementsByClassName(
                classSmallCircle
            )[0];
            const itemBig = document.getElementsByClassName(classBigCircle)[0];

            const {
                x: currentLeftPosition,
                y: currentTopPosition,
            } = itemSmall.getClientRects()[0];

            const rect = e.target.getBoundingClientRect();

            const cursorShiftGorizontal = Math.round(
                e.clientX - rect.left - rect.width / 2
            ); // 210 = 420 / 2; 420px width of title container
            const cursorShiftVertical = Math.round(
                e.clientY - rect.top - rect.height / 2
            );
            const position = currentLeftPosition + cursorShiftGorizontal;
            const positionVertical = currentTopPosition + cursorShiftVertical;

            itemSmall.style.left = position + "px";
            itemSmall.style.top = positionVertical + "px";

            itemBig.style.left = position + "px";
            itemBig.style.top = positionVertical + "px";
        }
    };

    const removeInlineStyles = (nameSmallCircle, nameBigCircle) => {
        document
            .getElementsByClassName(nameSmallCircle)[0]
            .removeAttribute("style");
        document
            .getElementsByClassName(nameBigCircle)[0]
            .removeAttribute("style");
    };

    return (
        <div className="index-page">
            <MainMobBlock />
            <MovingCursor />
            <IndexPageBlock
                openLink={openLink}
                moveCustomCircle={moveCustomCircle}
                removeInlineStyles={removeInlineStyles}
                isLinkOpened={isLinkOpened}
            />
        </div>
    );
};

export default IndexPage;
