import React, { Fragment } from "react";
import classnames from "classnames";

const IndexPageSircles = ({ isLinkOpened }) => {
    return (
        <Fragment>
            <img
                src={"http://placekitten.com/800/500"}
                className="index-page__img index-page__img--left"
                alt=""
            />
            <img
                src={"http://placekitten.com/700/500"}
                className="index-page__img index-page__img--right"
                alt=""
            />
            <img
                src={"http://placekitten.com/700/450"}
                className="index-page__img index-page__img--top"
                alt=""
            />
            <img
                src={"http://placekitten.com/900/700"}
                className="index-page__img index-page__img--bottom"
                alt=""
            />

            <div
                className={classnames(
                    "index-page__small-circle index-page__small-circle--left",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />
            <div
                className={classnames(
                    "index-page__small-circle index-page__small-circle--right",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />
            <div
                className={classnames(
                    "index-page__small-circle index-page__small-circle--top",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />
            <div
                className={classnames(
                    "index-page__small-circle index-page__small-circle--bottom",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />

            <div
                className={classnames(
                    "index-page__big-circle index-page__big-circle--left",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />
            <div
                className={classnames(
                    "index-page__big-circle index-page__big-circle--right",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />
            <div
                className={classnames(
                    "index-page__big-circle index-page__big-circle--top",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />
            <div
                className={classnames(
                    "index-page__big-circle index-page__big-circle--bottom",
                    {
                        "link-opened": isLinkOpened,
                    }
                )}
            />
        </Fragment>
    );
};

export default IndexPageSircles;
