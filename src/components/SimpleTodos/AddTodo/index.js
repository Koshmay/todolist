import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodoCreator } from "../../../redux/actionCreators";
import Input from "../../Common/Input";
import Button from "../../Common/Button";

const AddTodo = () => {
    const [todoValue, setTodoValue] = useState("");
    const dispatch = useDispatch();

    const onClickButton = () => {
        if (!todoValue || todoValue.trim() === "") {
            return;
        }
        dispatch(addTodoCreator(todoValue));
        setTodoValue("");
    };
    return (
        <div className="add-todo">
            <h2>ADD NEW TODO</h2>
            <Input
                value={todoValue}
                onChange={(e) => setTodoValue(e.target.value)}
            />{" "}
            <Button variant="success" onClick={onClickButton}>
                ADD
            </Button>
        </div>
    );
};

export default AddTodo;
