import React, { useState } from "react";
import { connect } from "react-redux";
import Input from "../../Common/Input";
import Button from "../../Common/Button";
import { addTodoCreator } from "../../../redux/actionCreators";

const AddTodo = ({ addNewTodo }) => {
    const [todoValue, setTodoValue] = useState("");

    const onClickButton = () => {
        if (!todoValue || todoValue.trim() === "") {
            return;
        }
        addNewTodo(todoValue);
        setTodoValue("");
    };
    return (
        <div className="add-todo">
            <h2>ADD NEW TODO</h2>
            <Input
                value={todoValue}
                onChange={(e) => setTodoValue(e.target.value)}
            />{" "}
            <Button variant="success" onClick={onClickButton}>
                ADD
            </Button>
        </div>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        addNewTodo: (data) => dispatch(addTodoCreator(data)),
    };
};

export default connect(null, mapDispatchToProps)(AddTodo);
