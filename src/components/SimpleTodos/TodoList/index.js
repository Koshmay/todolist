import React from "react";
import { useSelector, useDispatch } from "react-redux";
import classnames from "classnames";
import {
    changeTodoStatusCreator,
    removeTodoStatusCreator,
} from "../../../redux/actionCreators";
import Button from "../../Common/Button";
import "./index.css";

const TodoList = () => {
    const listTodos = useSelector((state) => state.todos.todolist);
    const dispatch = useDispatch();

    const changeStatus = (id) => {
        dispatch(changeTodoStatusCreator(id));
    };

    const remove = (id) => {
        dispatch(removeTodoStatusCreator(id));
    };
    return (
        <div className="todolist">
            <h3>TodoList</h3>
            <ul>
                {!listTodos.length && <h3>Todo List is Empty </h3>}
                {listTodos.length !== 0 &&
                    listTodos.map((item, index) => (
                        <li key={index} className={classnames("todo-li")}>
                            <span
                                onClick={() => changeStatus(item.id)}
                                className={classnames("todo-item", {
                                    "todo-item--done": item.status,
                                })}
                            >
                                {`${index + 1}. ${item.text}`}
                            </span>
                            <Button
                                onClick={() => remove(item.id)}
                                variant="warning"
                            >
                                DELETE
                            </Button>
                        </li>
                    ))}
            </ul>
        </div>
    );
};

export default TodoList;
