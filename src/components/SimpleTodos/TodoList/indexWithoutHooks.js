import React from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import {
    changeTodoStatusCreator,
    removeTodoStatusCreator,
} from "../../../redux/actionCreators";
import Button from "../../Common/Button";
import "./index.css";

const TodoList = ({ listTodos, changeTodo, removeTodo }) => {
    const changeStatus = (id) => {
        changeTodo(id);
    };
    const remove = (id) => {
        removeTodo(id);
    };
    return (
        <div className="todolist">
            <h3>TodoList</h3>
            <ul>
                {!listTodos.length && <h3>Todo List is Empty </h3>}
                {listTodos.length !== 0 &&
                    listTodos.map((item, index) => (
                        <li key={index} className={classnames("todo-li")}>
                            <span
                                onClick={() => changeStatus(item.id)}
                                className={classnames("todo-item", {
                                    "todo-item--done": item.status,
                                })}
                            >
                                {`${index + 1}. ${item.text}`}
                            </span>
                            <Button
                                onClick={() => remove(item.id)}
                                variant="warning"
                            >
                                DELETE
                            </Button>
                        </li>
                    ))}
            </ul>
        </div>
    );
};

const mapStateToProps = (state) => {
    const { todolist } = state.todos;
    return { listTodos: todolist };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeTodo: (data) => dispatch(changeTodoStatusCreator(data)),
        removeTodo: (data) => dispatch(removeTodoStatusCreator(data)),
    };
};

// like mapDispatchToProps
// const obj = {
//   changeTodo: changeTodoStatusCreator,
//   removeTodo : removeTodoStatusCreator
// }

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
