import React from "react";
import AddTodo from "../AddTodo";
import TodoList from "../TodoList";
import Navigation from "../../Common/Navigation";
import "./index.css";

const MainContainer = () => {
    return (
        <div className="main-container">
            <Navigation />
            <AddTodo />
            <br /> <br /> <br />
            <TodoList />
        </div>
    );
};

export default MainContainer;
